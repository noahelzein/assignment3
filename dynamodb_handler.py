from __future__ import print_function
import boto3
import json
import sys
import decimal
from boto3.dynamodb.conditions import Key, Attr

# Note - Do not change the class name and constructor
# You are free to add any functions to this class without changing the specifications mentioned below.
class DynamoDBHandler:

    def __init__(self, region):
        self.client = boto3.client('dynamodb')
        self.resource = boto3.resource('dynamodb', region_name=region)

    def help(self):
        print("Supported Commands:")
        print("1. insert_movie")
        print("2. delete_movie")
        print("3. update_movie")
        print("4. search_movie_actor")
        print("5. search_movie_actor_director")
        print("6. print_stats")
        print("7. delete_table")

    def create_and_load_data(self, tableName, fileName):
        # TODO - This function should create a table named <tableName> 
        # and load data from the file named <fileName>
        table = ''
        try:
            table = self.resource.create_table(
            TableName=tableName,
            KeySchema=[
                {
                    'AttributeName': 'year',
                    'KeyType': 'HASH'  #Partition key
                },
                {
                    'AttributeName': 'title',
                    'KeyType': 'RANGE'  #Sort key
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'year',
                    'AttributeType': 'N'
                },
                {
                    'AttributeName': 'title',
                    'AttributeType': 'S'
                },

            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 10,
                'WriteCapacityUnits': 10
                }
            )

            table.wait_until_exists()

            with open(fileName) as json_file:
                movies = json.load(json_file, parse_float = decimal.Decimal)
                for movie in movies:
                    year = int(movie['year'])
                    title = movie['title']
                    info = movie['info']
                    title_lower = title.lower()
                    actors_list = [] 
                    directors_list = []

                    print("Adding movie:", year, title)
                    
                    try:
                        if movie['info']['actors'] != '':
                            for actor in movie['info']['actors']:
                                actors_list.append(actor.lower())
                    except KeyError:
                        pass

                    try:
                        if movie['info']['directors'] != '':
                            for director in movie['info']['directors']:
                                directors_list.append(director.lower())
                    except KeyError:
                        pass

                    table.put_item(
                       Item={
                           'year': year,
                           'title': title,
                           'info': info,
                           'title_lower': title_lower,
                           'directors_list': directors_list,
                           'actors_list': actors_list
                        }
                    )
            
        except self.client.exceptions.ResourceInUseException:
            print("Table already created!")
            table = self.resource.Table(tableName)

        print("Table status:", table.table_status)

    def insert_movie(self, tableName):
        
        table = self.resource.Table(tableName)
        year = ''
        title = ''
        directors = ''
        actors = ''
        release_date = ''
        rating = ''
        ret_string = ''
        
        if sys.version_info[0] < 3:
                year = raw_input("Year>")
                title = raw_input("Title>")
                directors = raw_input("Directors>")
                actors = raw_input("Actors>")
                release_date = raw_input("Release date>")
                rating = raw_input("Rating>")
        else:
            year = input("Year>")
            title = input("Title>")
            directors = input("Directors>")
            actors = input("Actors>")
            release_date = input("Release date>")
            rating = input("Rating>")

        if year == '' or title == '':
            ret_string = 'Please insert required field(s): Year, Title.'
            return ret_string

        year = int(year)
        actors_list = [] 
        directors_list = []
        title_lower = title.lower()

        item_dict = {}
        info_dict = {}
        item_dict['year'] = year
        item_dict['title'] = title
        item_dict['title_lower'] = title_lower

        if directors or actors or release_date or rating != '':
            item_dict['info'] = info_dict

        if directors:
            directors = directors.split(",")
            for director in directors:
                directors_list.append(director.lower())
            item_dict['info']['directors'] = directors
            item_dict['directors_list'] = directors_list

        if actors:
            actors = actors.split(",")
            for actor in actors:
                actors_list.append(actor.lower())
            item_dict['info']['actors'] = actors
            item_dict['actors_list'] = actors_list

        if release_date:
            release_date = release_date.split(" ")
            item_dict['info']['release_date'] = release_date

        if rating != '':
            item_dict['info']['rating'] = rating
        
        table.put_item(
           Item=item_dict
        )

        ret_string = 'Movie ' + title + ' successfully inserted'
        return ret_string

    def delete_movie(self, tableName):
        table = self.resource.Table(tableName)
        title = ''
        found = False

        if sys.version_info[0] < 3:
                title = raw_input("Title>")
        else:
            title = input("Title>")

        real_title = title

        item_list = []
        title = title.lower()
        print(title)
        response = table.scan(
                              FilterExpression= Key('title_lower').eq(title),
                              ProjectionExpression="#yr, title",
                              ExpressionAttributeNames={ "#yr": "year"}
                              )
        if response['Items']:
            found = True
            for item in response['Items']: 
                item_list.append(item)
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                FilterExpression= Key('title_lower').eq(title),
                ProjectionExpression="#yr, title",
                ExpressionAttributeNames={ "#yr": "year"},
                ExclusiveStartKey=response['LastEvaluatedKey']
            )
            if response['Items']:
                found = True
                for item in response['Items']: 
                    item_list.append(item)

            
        if not found:
            return 'Movie ' + real_title + ' does not exist'

        for item in item_list:
            response = table.delete_item(
                        Key={
                            'year': item['year'],
                            'title': item['title']
                            }
                        )
        return 'Movie ' + real_title + ' successfully deleted'
    
    def update_movie(self, tableName):

        table = self.resource.Table(tableName)
        year = ''
        title = ''
        directors = ''
        actors = ''
        release_date = ''
        rating = ''
        update_expression = ''
        
        if sys.version_info[0] < 3:
                year = int(raw_input("Year>"))
                title = raw_input("Title>")
                directors = raw_input("Directors>")
                actors = raw_input("Actors>")
                release_date = raw_input("Release date>")
                rating = raw_input("Rating>")
        else:
            year = int(input("Year>"))
            title = input("Title>")
            directors = input("Directors>")
            actors = input("Actors>")
            release_date = input("Release date>")
            rating = input("Rating>")
        
        if year == '' or title == '':
            ret_string = 'Please insert required field(s): Year, Title.'
            return ret_string

        response = table.get_item(
            Key={
                'year': year,
                'title': title
            } 
        )

        if 'Item' not in response:
            return 'Movie with ' + title + ' and ' + str(year) + ' does not exist'
        
        year = int(year)
        actors_list = [] 
        directors_list = []
        found = False

        item_dict = {}

        if directors or actors or release_date or rating != '':
            update_expression = 'SET '

        if directors:
            found = True
            directors = directors.split(",")
            for director in directors:
                directors_list.append(director.lower())
            item_dict[':val3'] = directors
            item_dict[':val6'] = directors_list
            update_expression = update_expression + 'info.directors = :val3, directors_list = :val6'
            

        if actors:
            actors = actors.split(",")
            for actor in actors:
                actors_list.append(actor.lower())
            item_dict[':val1'] = actors
            item_dict[':val7'] = actors_list
            if found:
                update_expression = update_expression + ', info.actors = :val1, actors_list = :val7'
            else:
                update_expression = update_expression + 'info.actors = :val1, actors_list = :val7'
                found = True

        if release_date:
            release_date = release_date.split(" ")
            item_dict[':val4'] = release_date
            if found:
                update_expression = update_expression + ', info.release_date = :val4'
            else:
                update_expression = update_expression + 'info.release_date = :val4'
                found = True

        if rating != '':
            item_dict[':val2'] = rating
            if found:
                update_expression = update_expression + ', info.rating = :val2'
            else:
                update_expression = update_expression + 'info.rating = :val2'
                found = True

##        table.update_item(
##            Key={
##                'year': year,
##                'title': title
##            },
##            AttributeUpdates=item_dict 
##        )
            
        table.update_item(
            Key={
                'year': year,
                'title': title
            },
            UpdateExpression=update_expression,

            ExpressionAttributeValues=item_dict
        )

        return 'SUCCESS'

    def search_movie_actor(self, tableName):

        table = self.resource.Table(tableName)
        actor = ''

        if sys.version_info[0] < 3:
                actor = raw_input("Actor>")     
        else:
            actor = input("Actor>")

        real_actor = actor
        actor = actor.lower()
        response_list = []

        response = table.scan(
                               ProjectionExpression = "title, #yr, info.actors",
                               FilterExpression = "contains(#apps, :v)",
                               ExpressionAttributeNames = { "#apps": "actors_list", "#yr" : "year" },
                               ExpressionAttributeValues = { ":v": actor }
                              )
        if response['Items']:
            for item in response['Items']: 
                response_list.append(item)
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                               ProjectionExpression = "title, #yr, info.actors",
                               FilterExpression = "contains(#apps, :v)",
                               ExpressionAttributeNames = { "#apps": "actors_list", "#yr" : "year" },
                               ExpressionAttributeValues = { ":v": actor },
                               ExclusiveStartKey=response['LastEvaluatedKey']
                              )
            if response['Items']:
                for item in response['Items']: 
                    response_list.append(item)

        if not response_list:
            return ('No movies found for actor ' + real_actor)
        return response_list

    def search_movie_actor_director(self, tableName):

        table = self.resource.Table(tableName)
        actor = ''
        director = ''

        if sys.version_info[0] < 3:
                actor = raw_input("Actor>")
                director = raw_input("Director>")
        else:
            actor = input("Actor>")
            director = input("Director>")

        real_actor = actor
        real_director = director
        actor = actor.lower()
        director = director.lower()

        response_list = []

        response = table.scan(
                               ProjectionExpression = "title, #yr, info.actors, info.directors",
                               FilterExpression = "contains(#actors_list, :actor) AND contains(#directors_list, :director)",
                               ExpressionAttributeNames = {
                                                            "#actors_list": "actors_list",
                                                            "#directors_list": "directors_list",
                                                            "#yr" : "year"
                                                          },
                               ExpressionAttributeValues = {
                                                             ":actor": actor,
                                                             ":director": director
                                                           }
                              )

        if response['Items']:
            for item in response['Items']: 
                response_list.append(item)
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                               ProjectionExpression = "title, #yr, info.actors, info.directors",
                               FilterExpression = "contains(#actors_list, :actor) AND contains(#directors_list, :director)",
                               ExpressionAttributeNames = {
                                                            "#actors_list": "actors_list",
                                                            "#directors_list": "directors_list",
                                                            "#yr" : "year"
                                                          },
                               ExpressionAttributeValues = {
                                                             ":actor": actor,
                                                             ":director": director
                                                           },
                               ExclusiveStartKey=response['LastEvaluatedKey']
                              )
                                
            if response['Items']:
                for item in response['Items']: 
                    response_list.append(item)
                    
        if not response_list:
            return ('No movies found for actor ' + real_actor + ' and director ' + real_director)
        return response_list

    def print_stats(self, tableName):

        table = self.resource.Table(tableName)
        option = ''
        highest_option = "highest_rating_movie"
        lowest_option = "lowest_rating_movie"
        response_list = []
        target_value = 0
        result_list = []

        if sys.version_info[0] < 3:
                raw_input("print_stats>")
                option = raw_input("stats>")
                
        else:
            input("print_stats>")
            option = raw_input("stats>")

        if option == highest_option:
            
            response = table.scan(
                                   ProjectionExpression = "title, #yr, info.actors, info.rating",
                                   ExpressionAttributeNames = { "#yr" : "year" }
                                  )
            if response['Items']:
                for item in response['Items']: 
                    if 'info' in item:
                        if 'rating' in item['info']:
                            if float(item['info']['rating']) > float(target_value):
                                target_value = item['info']['rating']
            
            while 'LastEvaluatedKey' in response:
                response = table.scan(
                                   ProjectionExpression = "title, #yr, info.actors, info.rating",
                                   ExpressionAttributeNames = { "#yr" : "year" },
                                   ExclusiveStartKey=response['LastEvaluatedKey']                                
                                  )
                if response['Items']:
                    for item in response['Items']: 
                        if 'info' in item:
                            if 'rating' in item['info']:
                                if float(item['info']['rating']) > float(target_value):
                                    target_value = item['info']['rating']

        elif option == lowest_option:
            target_value = 10000000
            response = table.scan(
                                   ProjectionExpression = "title, #yr, info.actors, info.rating",
                                   ExpressionAttributeNames = { "#yr" : "year" }
                                  )
            if response['Items']:
                for item in response['Items']: 
                    if 'info' in item:
                        if 'rating' in item['info']:
                            if float(item['info']['rating']) < float(target_value):
                                target_value = item['info']['rating']
            
            while 'LastEvaluatedKey' in response:
                response = table.scan(
                                   ProjectionExpression = "title, #yr, info.actors, info.rating",
                                   ExpressionAttributeNames = { "#yr" : "year" },
                                   ExclusiveStartKey=response['LastEvaluatedKey']                                
                                  )
                if response['Items']:
                    for item in response['Items']: 
                        if 'info' in item:
                            if 'rating' in item['info']:
                                if float(item['info']['rating']) < float(target_value):
                                    target_value = item['info']['rating']
        else:
            return "Unrecognized statistic query."

        response = table.scan(
                                   FilterExpression= Key('info.rating').eq(target_value),
                                   ProjectionExpression = "title, #yr, info.actors, info.directors, info.rating",
                                   ExpressionAttributeNames = { "#yr" : "year" }
                                  )
        if response['Items']:
            for item in response['Items']: 
                result_list.append(item)

        while 'LastEvaluatedKey' in response:
                response = table.scan(
                                   FilterExpression= Key('info.rating').eq(target_value),
                                   ProjectionExpression = "title, #yr, info.actors, info.directors, info.rating",
                                   ExpressionAttributeNames = { "#yr" : "year" },
                                   ExclusiveStartKey=response['LastEvaluatedKey']                                
                                  )
                if response['Items']:
                    for item in response['Items']: 
                        result_list.append(item)

        return result_list

    def delete_table(self):

        tableName = ''

        if sys.version_info[0] < 3:
                raw_input("delete_table>")
                tableName = raw_input("table_name>")
                
        else:
            input("print_stats>")
            tableName = raw_input("table_name>")

        table = self.resource.Table(tableName)
        table.delete()

        return 'Table ' + tableName + ' successfully deleted'

    def dispatch(self, command_string, tableName):
        # TODO - This function takes in as input a string command (e.g. 'insert_movie')
        # the return value of the function should depend on the command
        # For commands 'insert_movie', 'delete_movie', 'update_movie', delete_table' :
        #       return the message as a string that is expected as the output of the command
        # For commands 'search_movie_actor', 'search_movie_actor_director', print_stats' :
        #       return the a list of json objects where each json object has only the required
        #       keys and attributes of the expected result items.

        # Note: You should not print anything to the command line in this function.
        response = ''

        if command_string == 'insert_movie':
            response = self.insert_movie(tableName)
        elif command_string == 'delete_movie':
            response = self.delete_movie(tableName)
        elif command_string == 'update_movie':
            response = self.update_movie(tableName)
        elif command_string == 'search_movie_actor':
            response = self.search_movie_actor(tableName)
        elif command_string == 'search_movie_actor_director':
            response = self.search_movie_actor_director(tableName)
        elif command_string == 'print_stats':
            response = self.print_stats(tableName)
        elif command_string == 'delete_table':
            response = self.delete_table()
        else:
            response = "Command not recognized."
        return response



def main():
    # TODO - implement the main function so that the required functionality for the program is achieved.

    db_handler = DynamoDBHandler(sys.argv[1])
    tableName = 'Movies'
    fileName = 'moviedata.json'

    db_handler.create_and_load_data(tableName, fileName)
    
    while True:
        try:
            command_string = ''
            if sys.version_info[0] < 3:
                command_string = raw_input("Enter command ('help' to see all commands, 'exit' to quit)>")
            else:
                command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
    
            # Remove multiple whitespaces, if they exist
            command_string = " ".join(command_string.split())
            
            if command_string == 'exit':
                print("Good bye!")
                exit()
            elif command_string == 'help':
                db_handler.help()
            else:
                response = db_handler.dispatch(command_string, tableName)
                print(response)
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main()
